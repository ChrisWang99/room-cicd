/*eslint no-unused-vars: "off" */
const chai = require("chai")
const expect = chai.expect
const request = require("supertest")
const mongoose = require("mongoose")
const Rooms = require("../../../models/rooms")
let  server = require("../../../bin/www")
mongoose.connect("mongodb://wang:wfb123@ds343217.mlab.com:43217/heroku_rrll436g")

describe("Rooms",  (done) => {
    beforeEach(async () => {
        try {
            await Rooms.deleteMany({})
            let room = new Rooms()
            room.roomtype = "twinRoom"
            room.price = 50
            room.roomNumber = 605
            room.available = 'available'
            await room.save()
            room = new Rooms()
            room.roomtype = "KingBedRoom"
            room.price = 60
            room.roomNumber = 714
            room.available = 'unavailable'
            await room.save()
        } catch (error) {
            console.log(error)
        }
    })
    describe("GET /rooms", () => {
        it(" should return all the rooms", done => {
            request(server)
                .get("/rooms")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .end((err, res) => {
                    try{
                        expect(200)
                        expect(res.body).to.be.a("array")
                        expect(res.body.length).to.equal(2)
                        expect(res.body[0]).to.include( { roomtype: "twinRoom",
                            roomNumber: 605 } )
                        expect(res.body[1]).to.include( { roomtype: "KingBedRoom",
                            roomNumber: 714  } )

                        done()
                    } catch(e){
                        done(e)
                    }
                })
        })
    })
    describe("GET /rooms/:roomNumber", () => {
        describe("when the room number is valid", () => {
            it("should return the matching room", () => {
                request(server)
                    .get(`/rooms/605`)
                    .expect(200)
                    .end((err, res) => {
                        expect(res.body[0]).to.include({roomtype: "twinRoom",
                            price: 50,
                            roomNumber: 605,
                            available: 'available'})
                        //done(err)
                    })
            })
        })
        describe("when the room number is invalid", () => {
            it("should return the NOT found message", done => {
                request(server)
                    .get("/rooms/none")
                    .set("Accept", "application/json")
                    .expect(200)
                    .end((err,res) =>{
                        expect(res.body.message).equals("Room NOT Found!")
                        done(err)
                    })
            })
        })
    })
    describe("POST /rooms", () => {
        it("should return confirmation message and update datastore", () => {
            const room = {
                roomtype: "single room",
                price: 45,
                roomNumber: 111,
                available: 'available'
            }
            return request(server)
                .post("/rooms")
                .send(room)
                .expect({ message: "Room Successfully changed!" })
        })
        after(() => {
            return request(server)
                .get("/rooms/111")
                .expect(200)
                .then(res => {
                    expect(res.body[0]).to.include( { roomtype: "single room",
                        roomNumber: 111 } )
                })
        })
    })
    describe("PUT /rooms/:roomNumber",  () => {
        describe("when the number is valid", () => {
            it("should return a message and change available", () => {
                const available = {available: "available"}
                return request(server)
                    .put(`/rooms/605`)
                    .send(available)
                    .expect(200)
                    .then(resp => {
                        expect(resp.body).to.include({
                            message: "Room Successfully changed!"
                        })
                    })
            })
            after(() => {
                return request(server)
                    .get(`/rooms/605`)
                    .set("Accept", "application/json")
                    .expect(200)
                    .then(resp => {
                        expect(resp.body[0]).to.have.property("available", "available")
                    })
            })
        })
        describe("when the number is invalid", () => {
            it("should return a 200", () => {
                return request(server)
                    .put("/rooms/none")
                    .expect(200)
            })
        })
    })
    describe("DELETE /rooms/:roomNumber", () => {
        it("should delete a room", () => {
            return request(server)
                .delete(`/rooms/605`)

                .expect(200)
                .then(res => {
                    expect(res.body).to.include({ message: "Room Deleted!"})

                })
        })
        after(() => {
            return request(server)
                .get("/rooms")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .then(res => {
                    expect(res.body).not.to.include({roomtype: "twinRoom",
                        price: 50,
                        roomNumber: 605,
                        available: 'available'})
                })
        })
    })
})


